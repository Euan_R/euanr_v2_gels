# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

include_directories(${UM_SOURCE_DIR}/gels/src)
include_directories(${UM_SOURCE_DIR}/nonlinear_elasticity/src)
include_directories(${UM_SOURCE_DIR}/nonlinear_elastic_materials/src)


add_executable(gel_analysis
  ${UM_SOURCE_DIR}/gels/gel_analysis.cpp
)

target_link_libraries(gel_analysis
  users_modules
  mofem_finite_elements
  mofem_interfaces
  mofem_multi_indices
  mofem_petsc
  mofem_approx
  mofem_third_party
  mofem_cblas
  boost_program_options
  ${MoFEM_PROJECT_LIBS}
)

add_test(gel_analysis_test
  ${MoFEM_MPI_RUN} -np 1 ${CMAKE_CURRENT_BINARY_DIR}/gel_analysis
  -my_file ${UM_SOURCE_DIR}/gels/meshes/brick_of_gel.cub
  -my_gel_config ${UM_SOURCE_DIR}/gels/gel_config.in
  -ksp_type fgmres -ksp_final_residual  -ksp_atol 1e-10 -ksp_rtol 1e-10 -ksp_monitor -ksp_converged_reason
  -pc_type lu -pc_factor_mat_solver_package mumps -snes_atol 1e-8 -snes_rtol 1e-8
  -snes_type newtonls -snes_linesearch_type basic
  -snes_linesearch_monitor -snes_monitor
  -ts_type beuler -ts_dt 0.1 -ts_final_time 0.1
  -ts_monitor
  -my_is_atom_test
)
