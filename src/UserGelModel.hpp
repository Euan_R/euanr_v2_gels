/** \file UserGelModel.hpp
  \brief Implementation of Gel finite element
  \ingroup gel
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */


#ifndef __UESRGELMODEL_HPP__
#define __UESRGELMODEL_HPP__

#ifndef WITH_ADOL_C
  #error "MoFEM need to be compiled with ADOL-C"
#endif

namespace GelModule {

/** \brief User (hackable) Gel model

  Member functions of \ref Gel::ConstitutiveEquation can be overloaded and
  user constitutive equation can be implemented. Since ADOL-C is used all
  tangent matrices are automatically calculated and physical nonlinearities easily
  added to the model.

*/
template<typename TYPE>
struct UserGelConstitutiveEquation: public Gel::ConstitutiveEquation<TYPE>  {

  UserGelConstitutiveEquation(map<int,Gel::BlockMaterialData> &data):
  Gel::ConstitutiveEquation<TYPE>(data) {
  }

  ublas::matrix<TYPE> E,C,S;

    double calLambda(const double G,const double nu) {
      return 0; // FIXME
    }

    double calMu(const double G,const double nu) {
      return 0; // FIXME
    }

    PetscErrorCode calC() {
      PetscFunctionBegin;
      C.resize(3,3,false);
      noalias(C) = prod(trans(F),F);
      PetscFunctionReturn(0);
    }

    PetscErrorCode calE() {
      PetscFunctionBegin;
      E.resize(3,3,false);
      noalias(E) = C;
      for(int ii = 0;ii!=3;ii++) {
        E(ii,ii) -= 1;
      }
      PetscFunctionReturn(0);
    }

    PetscErrorCode calS(const double lambda,const double mu) {
      PetscFunctionBegin;
      S.resize(3,3,false);
      TYPE tr = 0;
      for(int ii = 0;ii!=3;ii++) {
        tr += E(ii,ii);
      }
      noalias(S) = 2*mu*E;
      for(int ii = 0;ii!=3;ii++) {
        S(ii,ii) += lambda*tr;
      }
      PetscFunctionReturn(0);
    }


    PetscErrorCode calculateStressAlpha() {
      PetscFunctionBegin;
      ierr = calC(); CHKERRQ(ierr);
      ierr = calE(); CHKERRQ(ierr);
      double lambda = calLambda(dAta.gAlpha,dAta.vAlpha);
      double mu = calMu(dAta.gAlpha,dAta.vAlpha);
      ierr = calS(lambda,mu); CHKERRQ(ierr);
      stressAlpha.resize(3,3,false);
      noalias(stressAlpha) = prod(F,S);
      PetscFunctionReturn(0);
    }

    PetscErrorCode calculateStressBeta() {
      PetscFunctionBegin;
      ierr = calC(); CHKERRQ(ierr);
      ierr = calE(); CHKERRQ(ierr);
      double lambda = calLambda(dAta.gAlpha,dAta.vAlpha);
      double mu = calMu(dAta.gBeta,dAta.vBeta);
      ierr = calS(lambda,mu); CHKERRQ(ierr);
      stressAlpha.resize(3,3,false);
      noalias(stressBeta) = prod(F,S);
      PetscFunctionReturn(0);
    }


/*  PetscErrorCode calculateStressAlpha() {
    PetscFunctionBegin;
    this->traceStrainTotal = 0;
    for(int ii = 0;ii<3;ii++) {
      this->traceStrainTotal += this->strainTotal(ii,ii);
    }
    adouble w = exp((this->mU*6.02214129e23)/(8.314e3*298.15))/357.14286;
    adouble CI = 0.3898*exp(14.254*w) + ((1+tanh(1000*(w - 0.1874)))/2)*((0.0064*exp(36.179*w)) - (0.3898*exp(14.254*w)));
    adouble rgm1 = exp(-w);
    adouble rgm2 = exp(0.17*(CI - 1) - CI*w);
    this->stressAlpha.resize(3,3,false);
    adouble a = 2.0*this->dAta.gAlpha*(rgm1 + ((50*(1 + tanh(20*(w - 0.212)))/2))*(rgm1 - rgm2));
    adouble b = a*(this->dAta.vAlpha/(1.0-2.0*this->dAta.vAlpha));
    noalias(this->stressAlpha) = a*this->strainTotal;
    for(int ii = 0; ii<3; ii++){
      this->stressAlpha(ii,ii) += b*this->traceStrainTotal;
    }
    PetscFunctionReturn(0);
  }*/


  /*PetscErrorCode calculateStressBeta() {
    PetscFunctionBegin;
    this->traceStrainTotal = 0;
    for(int ii = 0;ii<3;ii++) {
      this->traceStrainTotal += this->strainTotal(ii,ii);
    }
    adouble w = exp((this->mU*6.02214129e23)/(8.314e3*298.15))/357.14286;
    adouble CI = 0.3898*exp(14.254*w) + ((1+tanh(1000*(w - 0.1874)))/2)*((0.0064*exp(36.179*w)) - (0.3898*exp(14.254*w)));
    adouble rgm1 = exp(-w);
    adouble rgm2 = exp(0.17*(CI - 1) - CI*w);
    this->stressBeta.resize(3,3,false);
    adouble a = 2.0*this->dAta.gBeta*(rgm1 + ((50*(1 + tanh(20*(w - 0.212))))/2)*(rgm1 - rgm2));
    adouble b = a*(this->dAta.vBeta/(1.0-2.0*this->dAta.vBeta));
    noalias(this->stressBeta) = a*this->strainTotal;
    for(int ii = 0; ii<3; ii++){
      this->stressBeta(ii,ii) += b*(this->traceStrainTotal-this->traceStrainHat);
    }
    PetscFunctionReturn(0);
  }*/

  /*PetscErrorCode calculateStrainHatFlux() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }*/

  /*PetscErrorCode calculateStressBetaHat() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }*/

  /*PetscErrorCode calculateSolventFlux() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }*/

 /*PetscErrorCode calculateSolventConcentrationDot() {
   PetscFunctionBegin;
   solventConcentrationDot = this->traceStrainTotalDot/ /*dAta.oMega*/ /*;
   PetscFunctionReturn(0);
 }*/

  /*PetscErrorCode calculateVolumeDot() {
    PetscFunctionBegin;
    volumeDot = traceStrainTotalDot;
    PetscFunctionReturn(0);
  }*/

};

}

#endif // __UESRGELMODEL_HPP__
